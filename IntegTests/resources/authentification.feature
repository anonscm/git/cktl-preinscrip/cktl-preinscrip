# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à l'application PreInscription
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
	
  Scénario: Authentification d'un utilisateur
    Soit Admin habilitée à utiliser PreInscription
    Lorsqu'elle se connecte avec le login grhumadm et le mot de passe "achanger!"
    Alors elle se retrouve sur la page d'accueil et voit le texte "Nom"
  
  
  